package com.epam.collections;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Collections {
    public static final String TEXT = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent venenatis vulputate neque sed laoreet. Quisque id cursus turpis. Aenean scelerisque, justo ut lobortis eleifend, nisi odio feugiat leo, nec aliquet nisl tortor ut orci. Ut facilisis at massa imperdiet laoreet. Sed laoreet leo risus, at congue est egestas in. Fusce mattis ullamcorper ligula, in semper metus lobortis eu. Aliquam rutrum leo sed interdum dignissim. In ullamcorper rhoncus nibh at mattis. In efficitur nisl porttitor sapien molestie, at ullamcorper erat lacinia. Sed vestibulum mi erat. Nullam id finibus nunc.\n" +
            "\n" +
            "Ut blandit libero id pulvinar consequat. Nam ut est neque. Sed sit amet malesuada velit. Vestibulum tincidunt placerat scelerisque. Etiam lacinia quam laoreet nulla consectetur, id laoreet libero ullamcorper. Nulla dictum augue in rhoncus vehicula. Nulla ut erat nec dolor tempor facilisis nec eu tellus. Nam at pharetra massa. Ut vehicula consectetur enim, eu porttitor ex. Interdum et malesuada fames ac ante ipsum primis in faucibus. In eu porta nisl, sit amet tempus metus. Integer lacinia elit id mattis congue. Sed felis arcu, congue et dui a, vulputate efficitur ipsum.\n" +
            "\n" +
            "Maecenas rhoncus id metus eu imperdiet. Donec venenatis, arcu vitae tempus tempus, urna nulla viverra justo, id porta leo odio nec orci. Vestibulum cursus tempor sollicitudin. Sed neque quam, pharetra vel mollis sed, luctus sed diam. Nulla feugiat turpis at purus sollicitudin tristique. Vivamus eleifend lacinia justo non imperdiet. Donec quis congue ligula, eu aliquet ipsum. Curabitur malesuada ante leo, ac iaculis sapien imperdiet at. Nullam gravida pulvinar sollicitudin. Quisque iaculis condimentum sapien, sit amet pharetra ipsum pellentesque gravida. In egestas, sem et convallis elementum, sapien ipsum aliquet est, sit amet scelerisque risus urna ut ligula. Sed ac purus eu lacus eleifend ultricies nec nec mauris. Cras ornare sodales massa, id sagittis sapien pellentesque ut. Vivamus fringilla dolor nec tempus bibendum. Curabitur vitae quam id neque fringilla cursus.\n" +
            "\n" +
            "Nulla facilisi. Vivamus aliquam metus non ipsum ultrices, vitae mollis nisi posuere. Maecenas dapibus sed risus ut consectetur. Morbi risus lacus, molestie vitae dolor in, faucibus tincidunt lacus. Morbi ultrices odio quis mauris vulputate fringilla. Ut dui mi, facilisis et ultrices vel, bibendum eu tortor. Sed hendrerit ultrices consectetur.\n" +
            "\n" +
            "Etiam maximus massa et varius scelerisque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi eu vulputate justo, et suscipit eros. Sed accumsan at enim consequat suscipit. Donec a justo vitae nibh consequat ultricies. Sed et metus diam. Nunc ut augue nec dolor venenatis efficitur.\n" +
            "\n" +
            "Aenean semper porta dui, ac porta velit venenatis ut. Phasellus eget placerat nisi. Duis auctor, sapien vitae rhoncus vulputate, sapien tortor laoreet lorem, sit amet facilisis nunc elit eget nulla. Maecenas mi ex, sagittis cursus tellus eu, condimentum suscipit lectus. Ut aliquet magna sit amet blandit semper. Nam eget congue nulla, sed euismod arcu. Mauris placerat sagittis neque vel sagittis. Praesent non massa in massa interdum consectetur. Aenean sagittis mi arcu, nec pharetra orci ultrices vel. Fusce quis efficitur tellus, eu euismod est. Vestibulum in arcu ac nunc rhoncus tincidunt et vel magna.\n" +
            "\n" +
            "Nam viverra hendrerit arcu non commodo. Sed dapibus felis non sem ultricies, placerat pharetra arcu accumsan. Donec efficitur tincidunt semper. Maecenas vulputate, nibh in facilisis sodales, urna quam viverra orci, quis pretium sapien mi non magna. Quisque purus justo, egestas id leo et, pellentesque tempus elit. Nullam id tincidunt ipsum. Mauris scelerisque laoreet enim nec consectetur. Etiam nec libero ultricies, ultricies turpis non, ullamcorper quam. Quisque volutpat mi arcu, et tempus augue ultrices vel. Nullam eget consectetur quam, nec vulputate ex. Sed nec mi metus. Praesent malesuada nec ante nec sagittis. Quisque condimentum magna risus, vel rhoncus augue tincidunt at.\n" +
            "\n" +
            "Proin non est et mauris sagittis lobortis ut vitae nulla. Sed quis bibendum sapien. Nulla sed congue ligula. Sed pharetra neque nunc, quis auctor ipsum finibus et. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas consequat mi tortor, quis pulvinar enim mollis at. Curabitur dapibus, urna blandit porta varius, sem tortor eleifend lectus, nec commodo sapien libero in dolor. Ut dignissim ex nec elit venenatis vestibulum. Donec varius volutpat maximus.\n" +
            "\n" +
            "Aliquam consectetur diam eget aliquam laoreet. Sed at urna sit amet turpis ullamcorper molestie. Nam nisi mi, ullamcorper sed massa et, tristique sagittis risus. Aliquam orci justo, commodo eget orci sit amet, consequat tincidunt justo. Curabitur et lacus urna. Sed ac pharetra turpis. Mauris pretium nisi eu urna tincidunt facilisis. Mauris rhoncus neque volutpat, molestie nisl a, fermentum ligula. Nam venenatis metus purus, vitae semper libero convallis ut. Aliquam facilisis mauris id faucibus varius. Vivamus a augue eu lacus pulvinar ultrices ut at risus. Quisque tincidunt turpis est, at euismod ipsum bibendum eget. Nullam id laoreet ipsum, sit amet imperdiet arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse tempus lorem nec libero semper, eu aliquam metus laoreet. Suspendisse dignissim neque ac metus volutpat, ut ultricies enim pretium.\n" +
            "\n" +
            "Donec tincidunt elementum mi quis vestibulum. Sed id nisi elementum, tincidunt nulla quis, blandit justo. Quisque nisl nisi, feugiat at vehicula ultrices, facilisis et quam. Vivamus at tincidunt elit, quis pulvinar dui. Maecenas non fermentum odio. Quisque in sapien id eros convallis feugiat. Fusce dapibus ex nec nibh maximus, sed ultrices purus eleifend. Nunc blandit imperdiet mauris, ac cursus lectus finibus quis.\n" +
            "\n" +
            "Proin ipsum mi, vestibulum quis lacinia ac, porta vitae lectus. Ut odio tellus, tincidunt at ex sit amet, aliquet mollis nibh. Nam varius, risus at ultricies pharetra, urna risus dictum tortor, eget scelerisque dui orci eget ante. Nulla ultrices scelerisque erat sed pulvinar. In lacinia ex eu lectus finibus efficitur. Fusce sit amet ligula lacinia, mollis lacus sit amet, cursus est. Praesent nunc leo, scelerisque mollis dignissim et, viverra non orci. Fusce ultrices sapien id risus consequat fermentum. Vivamus et tincidunt ligula.\n" +
            "\n" +
            "Nullam eu mollis leo. Phasellus bibendum tellus at aliquet egestas. Proin accumsan arcu ut sem tristique, nec aliquam nisl sodales. Nulla nec sagittis elit, eget ornare nulla. Nunc molestie dolor eget elementum suscipit. Aenean mollis felis at porta venenatis. Curabitur arcu sapien, rhoncus ut ultrices sit amet, interdum id justo. Maecenas sit amet posuere elit, ullamcorper venenatis arcu. Quisque non semper libero. Curabitur nec ligula nulla. Suspendisse facilisis tortor non augue dignissim convallis. Morbi sollicitudin erat vel massa vulputate, sed commodo quam tincidunt. Cras tempus, metus id feugiat sollicitudin, magna massa tincidunt sem, ut aliquam mauris purus ut augue. Morbi cursus aliquet lorem vitae malesuada. Aliquam mi justo, laoreet vitae arcu vel, faucibus feugiat leo. Suspendisse potenti.\n" +
            "\n" +
            "Integer dui mauris, lobortis vel porttitor at, sagittis in ligula. Fusce quis gravida diam. Ut a sapien pharetra, accumsan tellus vel, consequat metus. Vivamus massa diam, pulvinar sed tristique a, vestibulum ac risus. Suspendisse sed nibh gravida, consequat nibh ac, ullamcorper lorem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris egestas massa ipsum, pulvinar luctus arcu rutrum nec. Duis eget est non justo efficitur dignissim sit amet tempor justo. Nunc consectetur pharetra mi, a feugiat diam blandit ut. Fusce sollicitudin justo et elit tempus bibendum. Nunc semper dui ante, cursus aliquet diam luctus vitae. Vivamus porttitor maximus felis, interdum pharetra odio blandit ut. Phasellus libero justo, vehicula vitae est at, consequat rutrum ligula. Integer et blandit augue, nec rhoncus orci. Fusce malesuada, tellus ut vestibulum ullamcorper, nibh elit finibus tortor, ac pharetra libero ipsum eu dui. Proin nec condimentum est.\n" +
            "\n" +
            "Aenean quam odio, accumsan vitae sodales sit amet, interdum in urna. Etiam eleifend pretium purus porta ullamcorper. Curabitur a libero finibus, vestibulum diam a, luctus massa. Donec congue tincidunt molestie. Proin posuere sapien at dui blandit malesuada. Etiam nunc justo, pellentesque at turpis tempus, porttitor iaculis odio. In eleifend, dolor sed porttitor vulputate, nisi ante blandit magna, ac laoreet risus risus et mi. Vestibulum in ornare nulla, quis scelerisque eros. Nulla ullamcorper, nisi et rhoncus tincidunt, ante dui sodales lacus, elementum blandit risus neque ut leo. Donec nec tincidunt mi, vulputate tincidunt nibh. Curabitur posuere porta velit, ut tempor diam rutrum eget. Curabitur venenatis scelerisque tortor eu varius. Cras consequat nibh ornare sem suscipit aliquam. Vestibulum sagittis, dolor sed dignissim accumsan, odio leo varius tellus, at placerat elit justo eu ante. Mauris nec malesuada lectus. Mauris libero enim, sodales at neque vitae, egestas suscipit neque.\n" +
            "\n" +
            "Donec eget leo eget felis imperdiet dictum nec bibendum orci. Quisque egestas ullamcorper orci, at interdum erat hendrerit convallis. Integer pretium nisi id eros porttitor tincidunt. Nulla bibendum sit amet leo nec vehicula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse tincidunt, mi ut gravida pretium, odio ante malesuada enim, eu sollicitudin lorem massa commodo leo. Donec luctus justo mi, nec dignissim nulla volutpat a. Donec efficitur, nisl in posuere tincidunt, eros urna dictum nibh, in interdum sapien ipsum at diam. Sed sodales, elit nec blandit iaculis, massa sem imperdiet nunc, eget accumsan lorem augue nec mi. Donec ullamcorper libero at lacinia finibus. Nulla sed quam elit. Mauris porttitor at felis lacinia semper.";
    private static final String DELIMITER = "[^\\w]+";

    public static Map<String, Long> count(String text) {
        String[] s = text.split(DELIMITER);
        HashMap<String, Long> wordsCountMap = new HashMap<>();
        for (String word : s) {
            wordsCountMap.compute(word, (s1, count) -> count == null ? 1 : ++count);
        }
        return wordsCountMap;
    }

    public static List<String> getUniqueWord(String text) {
        return count(text).entrySet()
                .stream()
                .filter(entry -> entry.getValue() == 1)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    public static String sortWords(String text) {
        List<String> splittedText = Arrays.asList(text.split(DELIMITER));
        splittedText.sort(String::compareTo);
        return String.join(" ", splittedText);
    }

    public static void main(String[] args) {
        Map<String, Long> wordsCount = count(TEXT);
        System.out.println("Words count map: " + wordsCount);
        List<String> uniqueWords = getUniqueWord(TEXT);
        System.out.println("Unique words: " + uniqueWords);
        String sortedWords = sortWords(TEXT);
        System.out.println("Sorted words: " + sortedWords);
    }
}
